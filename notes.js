// Vue CLI - Lesson 17
// Create a dev environment workflow with webpack
// -- Use ES6 Features
// -- Compile & Minify our JS into 1 file
// -- Use single file templates
// -- Compile everything on our machine, not in a browser
// -- Live reload dev server

// Installation
            // template
// `vue init webpack-simple`

// LESSON 18 Vue files and Root Component
// main.js compiled the vue file as the component and
// render it to the Vue instance created

// LESSON 19 - Nesting Components
// Nesting Components
// -- Root Component
//      -- Header Component
//          -- Links Component
//          -- Login Component
//      -- Article Component
//      -- Footer Component

// ---- GLOBALLY ----
// 1. Create the Vue File for your compoent and structure it
// 2. Import to the `main.js`
// 3. register to the `Vue.component`
// 4. Call to the main vue file

// ---- LOCALLY ----
// 1. Import to the Vue you want to nest your components
// 2. Create an object from the `components` property
// 3. And register the imported component


// LESSON 20 - Component CSS
// Component should contain exactly one root element
// If you want your css to be a component based as well
// put `scoped` attributes to the style per component

// LESSON 21 - Nesting Components Examples
// -- Root Components
//      -- Header Component (Title)
//      -- Footer Component (Copyright Notice)
//      -- Pets Component (List of Pets)
// look for this example
import Header from './src/components/lesson21/Header.vue';
import Pets from './src/components/lesson21/Pets.vue';
import Footer from './src/components/lesson21/Footer.vue';

// LESSON 22 PROPS
// related on Lesson 21
// 2 Steps on passing and receiving props
// look for the App.vue and Pets.vue for some notes

// LESSON 23 Primitive vs Reference Types
// Primitive
// are strings, booleans and numbers
// Reference Types
// are object and arrays

// Passing a object or array as the data when you edit it to
// the nested component it will going to edit indirectly the original data 

// Premitive type
// Avoid mutating a prop directly since the value will
// be overwritten whenever the parent component re-renders. 
// Instead, use a data or computed property based on the prop's value.
// Prop being mutated: "title"