import Vue from 'vue';
import App from './App.vue'; // root component

// Vue.component(
//   ['pets', Pets],
//   ['footer', Header],
//   ['header', Footer],
// );

new Vue({ // Vue instance
  el: '#app',
  render: h => h(App) // Taking our root component which we import and render to the `#app` root element as our created instance
})
